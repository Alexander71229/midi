import javax.sound.midi.*;
import java.io.*;
import java.util.*;
public class M01{
	public static void main(String[]argumentos){
		try{
			String nombre="MuseNet improvises Beethoven from OP25 Ragtime03.midi";
			Sequence s=MidiSystem.getSequence(new File(nombre));
			long max=0;
			long min=15091;
			HashMap<Long,Integer>h=new HashMap<>();
			ArrayList<Long>a=new ArrayList<>();
			for(int i=0;i<s.getTracks().length;i++){
				Track t=s.getTracks()[i];
				for(int j=0;j<t.size();j++){
					MidiEvent me=t.get(j);
					ShortMessage sm=null;
					try{
						sm=(ShortMessage)me.getMessage();
						if(me.getMessage().getStatus()>=0x90&&me.getMessage().getStatus()<=0x9F){
							if(me.getTick()>max){
								max=me.getTick();
							}
							if(me.getTick()>=min){
								if(h.get(me.getTick())==null){
									a.add(me.getTick());
									h.put(me.getTick(),1);
								}
							}
						}
					}catch(Exception e){
					}
				}
			}
			for(int i=0;i<a.size();i++){
				System.out.println(a.get(i));
			}
			System.out.println(max);
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}