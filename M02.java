import javax.sound.midi.*;
import java.io.*;
public class M02{
	public static void main(String[]argumentos){
		try{
			String nombre="MuseNet improvises Beethoven from OP25 Ragtime03";
			String extension="midi";
			Sequence s=MidiSystem.getSequence(new File(nombre+"."+extension));
			Sequence r=new Sequence(s.getDivisionType(),s.getResolution(),2);
			long max=18085;
			long min=15150;
			for(int i=0;i<s.getTracks().length;i++){
				Track t=s.getTracks()[i];
				for(int j=0;j<t.size();j++){
					MidiEvent me=t.get(j);
					if(me.getTick()>=min&&me.getTick()<max){
						me.setTick(me.getTick()-min);
						r.getTracks()[i].add(me);
					}
				}
			}
			MidiSystem.write(r,1,new File(nombre+"-extract"+"."+extension));
			MidiDevice.Info[]ifs=MidiSystem.getMidiDeviceInfo();
			MidiDevice midiDevice=MidiSystem.getMidiDevice(ifs[0]);
			midiDevice.open();
			Receiver receiver=midiDevice.getReceiver();
			Sequencer sx=MidiSystem.getSequencer(false);
			sx.getTransmitter().setReceiver(receiver);
			sx.setSequence(r);
			sx.open();
			sx.start();
			System.in.read();
			sx.close();
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}